# The New Oil

## Mission Statement

- To **educate** readers on why privacy and security matter, and the various tools and techniques available to help them reclaim and protect their own privacy and security
- To **empower** readers to believe that privacy and security are attainable for everyone and to do their best to reclaim and protect their own privacy and security as much as necessary and possible for their unique situations.

## Contributing

Technology is a constantly changing field, and as people who take their privacy and security seriously, we place even more trust in the information on this site than most other technologies. As such, it is vital that The New Oil stays as current as possible. All readers are welcome to submit updates and suggested changes via a new [issue](https://gitlab.com/nbartram/the-new-oil/-/issues). Readers are also encouraged to join the [Matrix](https://matrix.to/#/+thenewoil:matrix.org) community for general discussion and meeting other like-minded privacy enthusiasts.

## Other Online Presences

- [Website](https://thenewoil.org/)
- [Deprecated Site URL](https://thenewoil.xyz/)
- [Mastodon](https://freeradical.zone/@thenewoil)
- [Blog](https://blog.thenewoil.org/)
- [Podcast](https://surveillancereport.tech/)
- [PeerTube](https://peertube.thenewoil.xyz/video-channels/thenewoil/videos)

## Support

In addition to submitting corrections and changes, here are some additional ways for readers to support The New Oil:

- [Bitcoin](https://thenewoil.org/btc.html): xpub661MyMwAqRbcGzNpy8KujhemxoFES1n7Sbc4JsxsJpCByqFGRVtxMXWpcLV318wxPj1YfuUeGbh2zWnLicad62pVKqXJbwMDorMt7vGV3gW
- [Monero](https://thenewoil.org/xmr.html): 44NAYMG1qupZcZ5WtjHWp58WNkXjetpTbRKHkVyrFZs8SiZK7SycQKdAm5y7sXuVhV1eYcShLjkKpbLRQSZaKxpvV2zo8ii
- [Brave Tips](https://support.brave.com/hc/en-us/articles/360021123971-How-do-I-tip-websites-and-Content-Creators-in-Brave-Rewards-)
- [Liberapay](https://liberapay.com/thenewoil)
- [Patreon](https://www.patreon.com/thenewoil)
- [PayPal](https://www.paypal.com/donate?hosted_button_id=NYJZS7AJ5X8NQ): Donations@TheNewOil.org
- Spreading the word and sharing the site with your friends and family!

_You can see how these donations will be used on our roadmap [here](https://thenewoil.org/roadmap.html)_

## License and Transparency

The New Oil is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

**[2020 Transparency Report](https://write.as/thenewoil/2020-recap-2021-plans)**

## Applications

- [Community Moderator Application](https://cryptpad.fr/form/#/2/form/view/99si-RTW4n6MV5i4wzzDuGpGSgQJ1mG8uoyi0q8z37M/)
- [Consulting Application](https://cryptpad.fr/form/#/2/form/view/vRN7JSx2x71E0Ufg7MthpP1ZeZSV7ZK0grbx-TlVlHc/)
